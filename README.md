Análisis del tratamiento de los residuos y desechos dentro de los hogares en las zonas urbanas del Ecuador.

Código fuente del análisis estadístico de datos en RStudio.

En la carpeta se encuentra el dataset BDD-MOD-AMB-HOGAR-ESPND-2019.sav, utilizado para el análisis respectivo, además de otra carpeta con los resultados obtenidos durante y finalizado el trabajo.
